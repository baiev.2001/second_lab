﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        { 
            Console.WriteLine("Print x1");
            double x1 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Print x1 Max");
            double x1_max = Double.Parse(Console.ReadLine());
            Console.WriteLine("Print delta x1");
            double d_x1 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Print x2");
            double x2 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Print x2 Max");
            double x2_max = Double.Parse(Console.ReadLine());
            Console.WriteLine("Print delta x2");
            double d_x2 = Double.Parse(Console.ReadLine());
            double sum = 0.0;
            Console.Write("X2\\X1\t");
            for (double x1_min = x1; x1_min <= x1_max; x1_min += d_x1)
            {
                Console.Write("{0:f4}\t", x1_min);
            }
            Console.WriteLine();
            for(double x2_min = x2; x2_min <= x2_max; x2_min += d_x2)
            {
                Console.Write("{0:f4}\t", x2_min);
                for (double x1_min = x1; x1_min <= x1_max; x1_min += d_x1)
                {
                    double y = 0.1 * x1_min * Math.Sin(x2_min) * Math.Cos(Math.Pow(x1_min, 4)) + 55;
                    Console.Write("{0:f4}\t", y);
                    double sum_sin = Math.Sin(y);
                    if (sum_sin > 0)
                    {
                        sum += sum_sin;
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("Sinus Sum: {0:f4}", sum);
        }
    }
}